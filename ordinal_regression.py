#! venv/bin/python
# the hope is that eventually this actually does ordinal regression

import numpy as np
import mysql.connector as mariadb
from sklearn import preprocessing, linear_model
from lightning.ranking import KernelPRank
import mord

connection = mariadb.connect(database='msqbstats', user='msqbstats')
cursor = connection.cursor()

cursor.execute("select r.ppb, r.powers, r.gets, r.negs, r.oppb, r.opowers, r.ogets, r.onegs, r.ppg, r.path, m.rank,"
               " r.name"
               " from results as r join tourneys as t on r.tourney = t.id"
               " join msnct_results as m on r.team = m.team and m.tourney_year = t.competition_year"
               " where r.path_criterion = 1 and m.tourney_year = 2011")

data = np.array(cursor.fetchall())
x = preprocessing.scale(data[:, :-2])
y = data[:, -2]
names = data[:, -1]

reg = KernelPRank()
reg.fit(x, y)

classifier = mord.LogisticAT()
