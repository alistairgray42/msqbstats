use msqbstats;

-- Delete results and tournaments using SSNCT, ICT, or HSNCT (usually not ms tourneys)
-- Also consider deleting where they use multiple sets, or IS sets, or idk
delete results,tourneys from results join tourneys on results.tourney = tourneys.id
where tourneys.qset regexp "CT";

-- Delete pseudonymous teams and chimeras
delete from results where team regexp "^[A-Z]$";

delete from results where name regexp "\\+|/";

-- Set path_criterion
update results as r
 join tourneys as t on t.id = r.tourney
  join msnct_results as m on m.team = r.team
  and m.tourney_year = r.competition_year
set r.path_criterion = 1
where r.path > .75 * m.path
and t.competition_year = m.tourney_year;