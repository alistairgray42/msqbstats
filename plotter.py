#! venv/bin/python

import mysql.connector as mariadb
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

connection = mariadb.connect(user='msqbstats', database='msqbstats')
cursor = connection.cursor()

# cursor.execute('select team,ppb,ppg,path from results order by ppb')
# rows = cursor.fetchall()
#
# rows = [[row[0], float(row[1]), row[2], row[3]] for row in rows]
#
# s = pd.DataFrame(rows, columns=['team', 'ppb', 'ppg', 'path'])
# s.sort_values(by='ppb')
#
# ppbs = np.array([row[1] for row in rows])
# ppgs = np.array([row[2] for row in rows])
#
# p = np.poly1d(np.polyfit(ppbs, ppgs, 2))
#
# s.plot(x='ppb', y='ppg', kind='scatter')
# plt.plot(ppbs, p(ppbs), "r-")
#
# plt.show()

cursor.execute('select ppb, powers, gets, negs, ppg, path from results as r where negs != 0')
               # ' join tourneys as t on r.tourney = t.id'
               # ' join msnct_results as m on r.team = m.team and t.competition_year = m.tourney_year'
               # ' where nats_result = 1')
rows = cursor.fetchall()
rows = [(float(row[0]), row[1], row[2], row[3], row[4], row[5],
         row[1] / row[3], row[2] / row[3], row[1] / (row[1] + row[2])) for row in rows
]

s = pd.DataFrame(rows, columns=['ppb', 'powers', 'gets', 'negs', 'ppg', 'path', 'p/n', 'g/n', 'p%'])
s.plot(x='gets', y='p%', kind='scatter')

# axes = pd.plotting.scatter_matrix(s, alpha=0.2)

plt.tight_layout()
plt.show()
