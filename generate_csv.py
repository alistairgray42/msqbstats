#! venv/bin/python
import mysql.connector as mariadb

connection = mariadb.connect(user='msqbstats', database='msqbstats')
cursor = connection.cursor()

for year in range(2011, 2019):

    with open('data/results/{}.csv'.format(year), 'w') as f:

        cursor.execute("select team, tourneys.name as tourney_name, qset, competition_year, ppb, powers, gets, negs,"
                       " oppb, opowers, ogets, onegs, path, path_criterion"
                       " from results join tourneys on results.tourney = tourneys.id"
                       " where (year(tourney_date) = {} and month(tourney_date) < 8)"
                       " or (year(tourney_date) = {} and month(tourney_date) > 7)"
                       .format(str(year), str(year - 1)))

        for c in cursor:
            s = "'{}','{}','{}',{},{},{},{},{},{},{},{},{},{},{}\n".format(
                c[0], c[1], c[2], str(c[3]), str(c[4]), c[5], c[6], c[7], str(c[8]), c[9], c[10], c[11], c[12], c[13])
            f.write(s)

    with open('data/nats/{}.csv'.format(year), 'w') as f:

        cursor.execute("select team, tourney_year, path, rank from msnct_results where tourney_year = {}".format(year))
        for c in cursor:
            s = "'{}',{},{},{}\n".format(*c)
            f.write(s)

