#! venv/bin/python
from bs4 import BeautifulSoup
import datetime
import urllib.request
import re
import mysql.connector as mariadb


def naqt_parse(url):
    """
    Given a BeautifulSoup object of an NAQT stats page, returns a list of division's
    stats.
    """

    content = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(content, 'html.parser')

    tourney_name = soup.h1.text
    date = soup.th.text

    connection = mariadb.connect(user='msqbstats', database='msqbstats')
    cursor = connection.cursor()

    # this is not a hyphen
    if "–" in date:
        date = date[date.index("–") + 2:]
    date = str(datetime.datetime.strptime(date, "%A, %B %d, %Y").date())

    (qset, schools, names, ppbs, powers, gets, negs, tuhs, oppbs, opowers, ogets, onegs, paths)\
        = naqt_parse_interior(soup)

    if not qset and not ppbs:
        print("Skipped {}!".format(tourney_name))
        return

    for i in range(len(schools)):
        tuh = tuhs[i]

        powers[i] /= tuh / 20
        gets[i] /= tuh / 20
        negs[i] /= tuh / 20

        opowers *= 20
        ogets *= 20
        onegs *= 20

    # for debugging
    # cursor.execute("truncate tourneys")
    # cursor.execute("truncate results")

    sql_tourneys_values = '("{0}","{1}","{2}")'.format(tourney_name, date, qset)
    print(sql_tourneys_values)
    cursor.execute("insert ignore into tourneys (name, tourney_date, qset) values {}".format(sql_tourneys_values))

    insert_id = cursor.lastrowid

    for i in range(len(schools)):

        sql_values = '("{0}","{1}","{2}",{3},{4},{5},{6},{7},{8},{9},{10},{11},{12})'.format(
            schools[i], names[i], insert_id, ppbs[i], powers[i], gets[i], negs[i],
            oppbs[i], opowers[i], ogets[i], onegs[i],
            15*powers[i] + 10*gets[i] - 5*negs[i] + ppbs[i]*(powers[i] + gets[i]),
            paths[i]
        )

        cursor.execute(
            "replace into results (team,name,tourney,ppb,powers,gets,negs,oppb,opowers,ogets,onegs,ppg,path) values {}"
            .format(sql_values))

    cursor.execute(
        "replace into sets (set_name) values ('{}')".format(qset)
    )

    connection.commit()

    return {"name": tourney_name, "date": date, "set": qset, "ppbs": ppbs, "powers": powers, "gets": gets, "negs": negs,
            "TUHs": tuhs, "oppbs": oppbs, "opowers": opowers, "ogets": ogets, "onegs": onegs}


def naqt_parse_interior(soup):
    """
    Given a BeautifulSoup object of a division's stats (not a full results page),
    return some tournament info
    """
    ppbs = []
    powers = []
    gets = []
    negs = []
    tuhs = []

    oppbs = []
    opowers = []
    ogets = []
    onegs = []

    paths = []

    names = []
    schools = []

    qset = "Unknown"
    division = ""

    for s in soup.findAll(["tr", "h2"]):

        # for some reason, the length found below is 5 for the two-column tables up top

        cells = s.findAll(["th", "td"])
        is_body_table = len(cells) > 5

        if not is_body_table and s.td is not None and s.td.string == "Packet Set:":
            qset = s.th.string
            continue

        if s.name == "h2":
            division = s.text
            continue

        if division and "Middle School" not in division and "MS" not in division:
            continue

        if "(" in division:
            qset = division[division.find("(") + 1: division.rfind(")")]

        is_table_header = s.findAll("th")[-1].has_attr("title")

        if is_body_table and is_table_header:
            for i in range(len(cells)):
                text = cells[i].string.strip("\n").strip("\t").strip(" ")

                if text == 'I':
                    negs_position = i
                elif text == 'TU':
                    gets_position = i
                elif text == 'P':
                    powers_position = i
                elif text == 'TUH':
                    tuh_position = i
                elif text == "PPB":
                    ppb_position = i
                    break

        if is_body_table and not is_table_header:
            name = s.a.text.replace('"', '')
            team_id = s.a['href']
            team_id = team_id[team_id.rfind("=") + 1:]
            # team_id being the per-tournament one and school_id being the permanent one

            if name == 'Bye' or name == 'Spoiler':
                continue

            team_soup = BeautifulSoup(urllib.request.urlopen(
                "https://www.naqt.com/stats/tournament/team.jsp?team_id={}".format(str(team_id))), 'html.parser'
            )

            stuff = oppo_stats(team_soup, team_id)
            if not stuff:
                continue

            (school_id, oppb, opower, oget, oneg) = stuff

            # teams without identifier letters are A teams
            if re.match(".+ [A-Z]$", name):
                school_id += name[-1]
            else:
                school_id += "A"
                name += " A"

            ppb = cells[ppb_position].text
            if ppb == " ":
                # nonbreaking space, when there's a forfeit game
                continue

            ppb = float(ppb)
            npowers = int(cells[powers_position].text)
            ngets = int(cells[gets_position].text)
            nnegs = int(cells[negs_position].text)
            tuh = int(cells[tuh_position].text)

            path = path_stats(team_soup, team_id, tuh)

            ppbs.append(ppb)
            powers.append(npowers)
            gets.append(ngets)
            negs.append(nnegs)
            tuhs.append(tuh)

            oppbs.append(oppb)
            opowers.append(opower)
            ogets.append(oget)
            onegs.append(oneg)
            paths.append(path)

            schools.append(school_id)
            names.append(name)

    if "(" in qset:
        qset = qset[:qset.rfind("(") - 1]

    return qset, schools, names, ppbs, powers, gets, negs, tuhs, oppbs, opowers, ogets, onegs, paths


def path_stats(soup, team_id, total_tuh):

    powers = dict()
    gets = dict()
    negs = dict()
    tuhs = dict()
    paths = dict()

    soup = soup.find("section", {"id": "team-{}-players".format(str(team_id))})

    for s in soup.findAll("tr"):
        cells = s.findAll(["th", "td"])

        is_table_header = s.findAll("th")[-1].has_attr("title")

        if is_table_header:
            for i in range(-1, -len(cells) - 1, -1):
                text = cells[i].string.strip("\n").strip("\t").strip(" ")

                if text == "I":
                    indiv_neg_position = i
                elif text == "TU":
                    indiv_gets_position = i
                elif text == "P":
                    indiv_powers_position = i
                elif text == "TUH":
                    indiv_tuh_position = i
                elif text == "Player":
                    indiv_name_position = i
                    break

        if not is_table_header:
            player_name = cells[indiv_name_position].text.strip("\n").strip("\t").strip(" ")
            powers[player_name] = int(cells[indiv_powers_position].text)
            gets[player_name] = int(cells[indiv_gets_position].text)
            negs[player_name] = int(cells[indiv_neg_position].text)
            tuhs[player_name] = int(cells[indiv_tuh_position].text)

    total_powers = sum(powers.values())
    total_gets = sum(gets.values())
    total_negs = sum(negs.values())

    for name in tuhs:

        player_powers = powers[name]
        player_gets = gets[name]
        player_negs = negs[name]
        player_tuh = tuhs[name]

        teammate_powers = total_powers - player_powers
        teammate_gets = total_gets - player_gets
        teammate_negs = total_negs - player_negs

        fraction_heard = player_tuh / total_tuh

        # part 1: total points of player
        part1 = 15 * player_powers + 10 * player_gets - 5 * player_negs

        # part 2: tuh, adjusted for teammates' buzzes
        part2 = player_tuh - (teammate_powers * fraction_heard) \
            - (teammate_gets * fraction_heard) - (0.5 * teammate_negs * fraction_heard)

        # note: this is not quite the same thing as is verbatim on qbwiki but that definition is whack yo
        path = 20 * part1 / part2
        paths[name] = path

    return sum(paths.values())


def oppo_stats(soup, team_id):

    school_id = ''

    soup_copy = soup
    soup_copy = soup_copy.find("section", {"id": "tournament-info"})

    for row in soup_copy.findAll("tr"):
        if row.td.text == "School:":
            school_id = row.th.a['href']
            school_id = school_id[school_id.rfind("=") + 1:]
            break

    soup = soup.find("section", {"id": "team-{}-games".format(str(team_id))})
    if not soup:
        return

    ppbs = 0
    powers = 0
    gets = 0
    negs = 0
    tuhs = 0

    count = 0

    for s in soup.findAll("tr"):

        cells = s.findAll(["th", "td"])

        is_body_table = len(cells) > 5  # 9
        is_table_header = s.findAll("th")[-1].has_attr("title")

        if is_body_table and is_table_header:
            for i in range(-1, -len(cells), -1):
                text = cells[i].string.strip("\n").strip("\t").strip(" ")

                if text == "PPB":
                    ppb_position = i
                elif text == "I":
                    negs_position = i
                elif text == "TU":
                    gets_position = i
                elif text == "P":
                    powers_position = i
                elif text == "TUH":
                    tuh_position = i
                    break

        if is_body_table and not is_table_header:

            ppb = cells[ppb_position].text
            if ppb == " ":
                # nonbreaking space, when there's a forfeit game
                continue

            ppb = float(ppb)
            npowers = int(cells[powers_position].text)
            ngets = int(cells[gets_position].text)
            nnegs = int(cells[negs_position].text)
            tuh = int(cells[tuh_position].text)

            ppbs += ppb
            powers += npowers
            gets += ngets
            negs += nnegs
            tuhs += tuh

            count += 1

    if count == 0:
        return
    return school_id, ppbs / count, powers / tuhs, gets / tuhs, negs / tuhs


def msnct_parse(url):

    content = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(content, 'html.parser')
    soup_copy = soup

    date = soup.th.text

    connection = mariadb.connect(user='msqbstats', database='msqbstats')
    cursor = connection.cursor()

    # this is not a hyphen
    if "–" in date:
        date = date[date.index("–") + 2:]
    year = str(datetime.datetime.strptime(date, "%A, %B %d, %Y").date().year)

    stuff = naqt_parse_interior(soup)
    paths = stuff[-1]
    teams = stuff[1]

    count = 0

    for row in soup_copy.findAll("tr"):
        cells = row.findAll(["td", "th"])

        is_body_table = len(cells) > 5  # 9
        is_table_header = row.findAll("th")[-1].has_attr("title")

        if is_body_table and not is_table_header:
            rank = cells[0].text.strip("\n").strip("\t").strip(" ")
            team_name = cells[1].a.text.strip("\n").strip("\t").strip(" ").strip('"')

            team = teams[count]

            if rank == "?":
                continue

            cursor.execute(
               'replace into msnct_results (team, team_name, tourney_year, path, rank) values ("{}","{}",{},{},{})'.format(
                team, team_name, year, paths[count], rank
                ))
            count += 1

    connection.commit()


# print(json.dumps(naqt_parse("https://www.naqt.com/stats/tournament/standings.jsp?tournament_id=8654"),
#                  indent=4, separators=(',', ': ')))
# naqt_parse("https://www.naqt.com/stats/tournament/standings.jsp?tournament_id=5611")

for year in range(2010, 2018):
    landing_url = "https://www.naqt.com/stats/tournament/?audience_id=1001&year_code={}".format(year)

    landing_soup = BeautifulSoup(urllib.request.urlopen(landing_url).read(), 'html.parser')

    for row in landing_soup.findAll("tr")[1:]:
        cells = row.findAll(['td', 'th'])
        if cells[3].text == "Complete" and "National Championship" not in cells[1].text:
            url = "https://www.naqt.com" + row.a['href']
            try:
                naqt_parse(url)
            except UnboundLocalError:
                print("skipped! " + url)

        elif "Middle School National Championship" in cells[1].text:
            url = "https://www.naqt.com" + row.a['href']
            msnct_parse(url)

# for id in [3484, 3924, 4299, 4999, 5699, 6599, 7399, 8399]:
#     msnct_parse("https://www.naqt.com/stats/tournament/standings.jsp?tournament_id={}".format(id))
#     print(id)
